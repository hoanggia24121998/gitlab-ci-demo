package com.example.demo.servicetest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.mockito.ArgumentMatchers.any;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.example.demo.dto.ProductDTO;
import com.example.demo.exception.InvalidParameterException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.model.Products;
import com.example.demo.repository.ProductRepository;

import com.example.demo.service.serviceimpl.ProductServiceImpl;

@SpringBootTest(classes = ProductServiceImplTest.class)
class ProductServiceImplTest {
    
    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductMapper productMapper;

    @InjectMocks
    private ProductServiceImpl productService;

    private static List<Products> productList;

    @BeforeAll
    static void setUpBeforeClass() {
        productList = new ArrayList<>();
        productList.add(new Products(1L, "product1", 100, 1));
        productList.add(new Products(2L, "product2", 200, 2));
    }

    @Test
    void testGetAllProducts() {

        when(productRepository.findAll()).thenReturn(productList);

        List<Products> result = productService.getAllProducts();

        assertNotNull(result);
        assertEquals(2, result.size());
    }

    @Test
    void testGetProductWithId_ProductFound() {

        Products product = new Products();
        product.setId(1L); 
    
        when(productRepository.findById(1L)).thenReturn(Optional.of(product));

        ProductDTO productDTO = new ProductDTO();
        productDTO.setName("Product1"); 
    

        when(productMapper.toProductDTO(product)).thenReturn(productDTO);

        ProductDTO result = productService.getProductWithId(1L);
    
        assertNotNull(result);
        assertEquals("Product1", result.getName());
    }

    @Test
    void testGetProductWithId_ProductNotFound() {

        when(productRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> {
            productService.getProductWithId(1L);
        });
    }

    @Test
    void testDeleteProductWithId_ProductFound(){
        Products product = new Products();
        product.setId(1L);

        when(productRepository.findById(1L)).thenReturn(Optional.of(product));

        ResponseEntity<?> response= productService.deleteProducts(1L);

        verify(productRepository, times(1)).delete(Optional.of(product).get());
        assertEquals(204, response.getStatusCodeValue());

    }

    @Test
    void testDeleteProductWithId_ProductNotFound() {
        when(productRepository.findById(1L)).thenReturn(Optional.empty());
    
        assertThrows(NotFoundException.class, () -> {
            productService.deleteProducts(1L);
        });
    }

    @Test 
    void testCreateProducts_ProductDoesNotExist(){
        ProductDTO productDTO = new ProductDTO();
        productDTO.setName("newProduct");

        when(productRepository.findByName("newProduct")).thenReturn(null);

        Products savepProducts = new Products();
        savepProducts.setName("newProduct");

        when(productMapper.toProducts(productDTO)).thenReturn(savepProducts);

        when(productRepository.save(savepProducts)).thenReturn(savepProducts);

        Products result = productService.createProducts(productDTO);

        assertNotNull(result);
        assertEquals("newProduct",result.getName());
    }


    @Test
    void testPartialUpdateProduct_Success() {

        Products existingProduct = new Products();
        existingProduct.setId(1L);
        existingProduct.setName("SampleProduct");
        existingProduct.setPrice(100.0);


        when(productRepository.findById(1L)).thenReturn(Optional.of(existingProduct));

        Map<Object, Object> fieldsToUpdate = new HashMap<>();
        fieldsToUpdate.put("price", 200.0);

        Products updatedProduct = new Products();
        updatedProduct.setName("SampleProduct");
        updatedProduct.setPrice(200.0);

        when(productRepository.save(any(Products.class))).thenReturn(updatedProduct);

        Products result = productService.partialUpdateProduct(1L, fieldsToUpdate);


        assertEquals(updatedProduct.getName(), result.getName());
        assertEquals(updatedProduct.getPrice(), result.getPrice());
        verify(productRepository, times(1)).save(any(Products.class));
    }

    @Test
    void testPartialUpdateProduct_NotFound() {

        when(productRepository.findById(1L)).thenReturn(Optional.empty());

        Map<Object, Object> updateFields = new HashMap<>();
        updateFields.put("price", 150.0);

        assertThrows(NotFoundException.class, () -> {
            productService.partialUpdateProduct(1L, updateFields);
        });

    }

    @Test
    void testPartialUpdateProductWithNullField() {
        Long id = 1L;
        Map<Object, Object> fields = new HashMap<>();
        fields.put("invalidField", "NewValue"); 

        when(productRepository.findById(id)).thenReturn(Optional.of(new Products()));

        assertThrows(InvalidParameterException.class, () -> {
            productService.partialUpdateProduct(id, fields);
        });
    }
}
