package com.example.demo.servicetest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.dto.UserDTO;
import com.example.demo.exception.NotFoundException;
import com.example.demo.mapper.UserMapper;
import com.example.demo.model.Users;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.serviceimpl.UserServiceImpl;

@SpringBootTest(classes = UserServiceImplTest.class)
class UserServiceImplTest {
    
    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserServiceImpl userService;

    private static List<Users> userList;

    @BeforeAll
    static void setUpBeforeClass() {
        userList = new ArrayList<>();
        userList.add(new Users(1L, "user1", "user1@toshiba", "111"));
        userList.add(new Users(2L, "user2", "user2@toshiba", "111"));
    }

    @Test
    void testGetAllUser(){

        when(userRepository.findAll()).thenReturn(userList);
        List<Users> result = userService.getAllUser();
        assertEquals(2, result.size());
    }

    @Test
    void testGetUserWithId_IdFound() {

        Users user = new Users();
        user.setId(1L); 
        user.setUsername("exampleUser"); 
    
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
    
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername(user.getUsername());
    

        when(userMapper.toUserDTO(user)).thenReturn(userDTO);

        UserDTO result = userService.getUserWithId(1L);
    
    
        assertNotNull(result);
        assertEquals(user.getUsername(), result.getUsername());
    }
    

    @Test 
    void testGetUserWithName_UserNotFound(){
        when(userRepository.findById(1L)).thenReturn(Optional.empty());
        
        assertThrows(NotFoundException.class, () -> {
            userService.getUserWithId(1L);
        });
    }

    @Test
    void testCreateUsers_UserDoesNotExist() {

        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("newUser");

        when(userRepository.findByUsername("newUser")).thenReturn(null);


        Users savedUser = new Users();
        savedUser.setUsername("newUser");

        when(userMapper.toUsers(userDTO)).thenReturn(savedUser);

        when(userRepository.save(savedUser)).thenReturn(savedUser);

        Users result = userService.createUsers(userDTO);

        assertNotNull(result);
        assertEquals("newUser", result.getUsername());
    }

}
