package com.example.demo.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.dto.ProductDTO;
import com.example.demo.model.Products;

@Component
public class ProductMapper{

    @Autowired
	private ModelMapper modelMapper;

    public ProductDTO toProductDTO(Products products){
        if(products == null){
            return null;
        }
        return this.modelMapper.map(products,ProductDTO.class);
    }

    public Products toProducts(ProductDTO productDTO){
        if(productDTO == null){
            return null;
        }
        return this.modelMapper.map(productDTO,Products.class);
    }
    
}
