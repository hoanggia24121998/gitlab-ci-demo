package com.example.demo.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.example.demo.dto.UserDTO;

import com.example.demo.model.Users;

@Component
public class UserMapper {
    @Autowired
	private ModelMapper modelMapper;

    public UserDTO toUserDTO(Users users){
        if(users== null){
            return null;
        }
        return this.modelMapper.map(users,UserDTO.class);
    }

    public Users toUsers(UserDTO userDTO){
        if(userDTO == null){
            return null;
        }
        return this.modelMapper.map(userDTO,Users.class);
    }  
}
