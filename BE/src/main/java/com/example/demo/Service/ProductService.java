package com.example.demo.service;

import java.util.List;
import java.util.Map;


import org.springframework.http.ResponseEntity;

import com.example.demo.dto.ProductDTO;
import com.example.demo.model.Products;

public interface ProductService {
    List<Products> getAllProducts();
    ProductDTO getProductWithId(Long id);
    Products createProducts(ProductDTO productDto);
    Products partialUpdateProduct(Long id,Map<Object, Object> fields);
    ResponseEntity<?> deleteProducts(Long id);
}
