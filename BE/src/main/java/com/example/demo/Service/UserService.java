package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.UserDTO;
import com.example.demo.model.Users;

public interface UserService {
    List<Users> getAllUser();
    UserDTO getUserWithId(Long id);
    Users createUsers(UserDTO userDto);
}
