package com.example.demo.service.serviceimpl;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import com.example.demo.dto.ProductDTO;
import com.example.demo.exception.InvalidParameterException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.model.Products;
import com.example.demo.repository.ProductRepository;
import com.example.demo.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepository productRepository;

    @Autowired
	private ProductMapper productMapper;

    @Override
    public List<Products> getAllProducts() {
        return productRepository.findAll() ;
    }

    @Override
    public ProductDTO getProductWithId(Long id) {
        Optional<Products> product = productRepository.findById(id);
        if (product.isPresent()) {
            return productMapper.toProductDTO(product.get());
        } else {
            throw new NotFoundException("Product not found with id: " + id);
        }
    }

    @Override
    public Products createProducts(ProductDTO productDto) {
        return productRepository.save(productMapper.toProducts(productDto));
    }

    @Override
    public Products partialUpdateProduct(Long id, Map<Object, Object> fields) {
        Optional<Products> existingProductOptional = productRepository.findById(id);
        if (existingProductOptional.isPresent()) {
            Products existingProduct = existingProductOptional.get();
            fields.forEach((key, value) -> {
                Field field = ReflectionUtils.findField(Products.class, (String) key);
                if (field != null) {
                    field.setAccessible(true);
                    ReflectionUtils.setField(field, existingProduct, value);
                } else {
                    throw new InvalidParameterException("Field '" + key + "' is not a valid field for Product.");
                }
            });
            productRepository.save(existingProduct); 
            return existingProduct;
        } else {
            throw new NotFoundException("Product with id " + id + " not found.");
        }
    }
    

    @Override
    public ResponseEntity<?> deleteProducts(Long id) {
        Optional<Products> products = productRepository.findById(id);
        if (products.isPresent() ) {
            productRepository.delete(products.get());
            return ResponseEntity.noContent().build();
        } else {
            throw new NotFoundException("Product not found with name: " + id);
        }
    }
}
