package com.example.demo.service.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.UserDTO;
import com.example.demo.exception.NotFoundException;
import com.example.demo.mapper.UserMapper;
import com.example.demo.model.Users;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserMapper userMapper;

    @Override
    public List<Users> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public UserDTO getUserWithId(Long id) {
        Optional<Users> users = userRepository.findById(id);
        if (!users.isPresent()) {
            throw new NotFoundException("User not found with id: " + id);
        }
        return userMapper.toUserDTO(users.get());
    }

    @Override
    public Users createUsers(UserDTO userDto) {
         return userRepository.save(userMapper.toUsers(userDto));
    }
    
}
