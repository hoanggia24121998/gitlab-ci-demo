package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.UserDTO;
import com.example.demo.model.Users;
import com.example.demo.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("/cicd/users")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/all")
    public ResponseEntity<List<Users>> getAllUser(){
        userService.getAllUser();
        return ResponseEntity.ok(userService.getAllUser());
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getUserWithName(@RequestParam Long id){
        return ResponseEntity.ok(userService.getUserWithId(id));
    }

    @PostMapping
    public ResponseEntity<Users> createUsers(@RequestBody UserDTO userDto){
        return ResponseEntity.ok(userService.createUsers(userDto));
    }
}
