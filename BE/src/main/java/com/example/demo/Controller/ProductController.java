package com.example.demo.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ProductDTO;
import com.example.demo.model.Products;
import com.example.demo.service.ProductService;

@CrossOrigin
@RestController
@RequestMapping("/cicd/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/all")
    public ResponseEntity<List<Products>> getAllProducts(){
        return ResponseEntity.ok(productService.getAllProducts());
    }

    @GetMapping("{id}")
    public ResponseEntity<ProductDTO> getProductWithName(@PathVariable Long id){
        ProductDTO productDTO = productService.getProductWithId(id);
        if (productDTO != null) {
            return ResponseEntity.ok(productDTO);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<Products> createProducts(@RequestBody ProductDTO productDto){
        return ResponseEntity.ok(productService.createProducts(productDto));
    }

    @PatchMapping("{id}")
    public ResponseEntity<Products> partialUpdateProduct(@PathVariable Long id, @RequestBody Map<Object, Object> fields) {
        Products product = productService.partialUpdateProduct(id, fields);
        if (product  != null) {
            return ResponseEntity.ok(product);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProducts(@PathVariable Long id){
        productService.deleteProducts(id);
        return ResponseEntity.ok("Product deleted successfully");
    }
}

