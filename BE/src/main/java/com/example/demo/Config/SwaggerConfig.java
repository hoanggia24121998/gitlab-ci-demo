package com.example.demo.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;


@Configuration
public class SwaggerConfig {
    @Value("${openapi.dev-url}")
	private String devUrl;

	@Value("${openapi.prod-url}")
	private String prodUrl;

	@Bean
	public OpenAPI myOpenAPI() {
		String url = "https://";
		Server devServer = new Server();
		devServer.setUrl(devUrl);
		devServer.setDescription("Dev environment");

		Server prodServer = new Server();
		prodServer.setUrl(prodUrl);
		prodServer.setDescription("Prod environment");

		Contact contact = new Contact();
		contact.setEmail("email@email");
		contact.setName("TSDV");
		contact.setUrl(url);

		License mitLicense = new License().name("MIT License").url(url);
		Info info = new Info().title("CICD Management API").version("1.0").contact(contact)
				.description("This API exposes endpoints.").termsOfService(url).license(mitLicense);
		return new OpenAPI().info(info).servers(Arrays.asList(devServer, prodServer));
	}
}
