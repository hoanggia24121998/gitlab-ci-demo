package com.example.demo.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(NotFoundException.class)
	public ResponseEntity<Object> handleUserNotFoundException(NotFoundException e) {
		ApiException apiException = new ApiException(e.getMessage(), HttpStatus.BAD_REQUEST);
		return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(InvalidParameterException.class)
	public ResponseEntity<Object> handleInvalidParameterException(InvalidParameterException e){
		ApiException apiException = new ApiException(e.getMessage(),HttpStatus.BAD_REQUEST);
		return new ResponseEntity<>(apiException,HttpStatus.BAD_REQUEST);
	}
}
