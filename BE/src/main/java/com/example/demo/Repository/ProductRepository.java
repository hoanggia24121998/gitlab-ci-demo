package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Products;

public interface ProductRepository extends JpaRepository<Products, Long> {

    Products findByName(String nameProduct);

    void delete(Products product);

    void save(Optional<Products> existingProduct);
}
