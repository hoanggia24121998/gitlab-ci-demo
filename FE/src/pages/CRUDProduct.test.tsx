import { render, screen, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { describe } from 'vitest';
import { MemoryRouter } from 'react-router-dom';
import CRUDProducts from './CRUDProducts';

const queryClient = new QueryClient();

const customRender = () => {
  return render(
    <QueryClientProvider client={queryClient}>
      <MemoryRouter>
        <CRUDProducts />
      </MemoryRouter>
    </QueryClientProvider>
  );
};

describe('Should display mainscreen Product', () => {
  describe('Should display text/style of Product screen ', () => {
    test('should display text header all the time', async () => {
      customRender();
      await waitFor(() => {
        const element = screen.getByText('Products Page');
        expect(element).toBeInTheDocument();
      });
    });
    test('should display style header all the time', async () => {
      customRender();
      await waitFor(() => {
        const element = screen.getByText('Products Page');
        expect(element).toHaveClass(
          'flex-grow',
          'font-semibold',
          'text-center',
          'text-5xl'
        );
      });
    });
  });
  describe('Should display button of Product screen all the time', () => {
    test('should display text Add product of button all the time', async () => {
      customRender();
      await waitFor(() => {
        const element = screen.queryByRole('button', { name: 'Add product' });
        expect(element).toBeInTheDocument();
      });
    });
    test('should display style Add product of button all the time', async () => {
      customRender();
      await waitFor(() => {
        const element = screen.queryByRole('button', { name: 'Add product' });
        expect(element).toHaveClass(
          'mr-4',
          'py-2',
          'px-4',
          'rounded',
          'text-white'
        );
      });
    });
  });
  describe('Should display table of main screen all the time', () => {
    test('should display text ID of table all the time', async () => {
      customRender();
      await waitFor(() => {
        const element = screen.getByRole('columnheader', { name: 'ID' });
        expect(element).toBeInTheDocument();
      });
    });
    test('should display text NAME of table all the time', async () => {
      customRender();
      await waitFor(() => {
        const element = screen.getByRole('columnheader', { name: 'NAME' });
        expect(element).toBeInTheDocument();
      });
    });
    test('should display text PRICE of table all the time', async () => {
      customRender();
      await waitFor(() => {
        const element = screen.getByRole('columnheader', { name: 'PRICE' });
        expect(element).toBeInTheDocument();
      });
    });
    test('should display text AMOUNT of table all the time', async () => {
      customRender();
      await waitFor(() => {
        const element = screen.getByRole('columnheader', { name: 'AMOUNT' });
        expect(element).toBeInTheDocument();
      });
    });
    test('should display text INFOR of table all the time', async () => {
      customRender();
      await waitFor(() => {
        const element = screen.getByRole('columnheader', { name: 'INFOR' });
        expect(element).toBeInTheDocument();
      });
    });
    test('should display number of table', async () => {
      customRender();
      await waitFor(() => {
        const element = screen.queryAllByRole('columnheader');
        expect(element.length).toBe(5);
      });
    });
    test('should display table with expected rows', async () => {
      await customRender();
      waitFor(async () => {
        const rows = await screen.findAllByRole('row');
        expect(rows).toHaveLength(10);
      });
    });
  });
});
