import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { describe } from 'vitest';
import { MemoryRouter } from 'react-router-dom';
import AddEditProduct from './AddEditProduct';

const queryClient = new QueryClient();

const customRender = () => {
  return render(
    <QueryClientProvider client={queryClient}>
      <MemoryRouter>
        <AddEditProduct />
      </MemoryRouter>
    </QueryClientProvider>
  );
};

describe('Display Add/Edit Product', () => {
  test('should display text Tag Name all the time', async () => {
    customRender();
    await waitFor(() => {
      const element = screen.getByLabelText('Name');
      expect(element).toBeInTheDocument();
    });
  });
  test('should display Tag Name input when user type', async () => {
    customRender();
    await waitFor(() => {
      const inputElement = screen.getByPlaceholderText(
        'enter your name'
      ) as HTMLInputElement;
      expect(inputElement).toBeInTheDocument();
      fireEvent.change(inputElement, { target: { value: 'Sample Name' } });
      expect(inputElement.value).toBe('Sample Name');
    });
  });
  test('should display text Tag Price all the time', async () => {
    customRender();
    await waitFor(() => {
      const element = screen.getByLabelText('Price');
      expect(element).toBeInTheDocument();
    });
  });
  test('should display Tag Price input when user type', async () => {
    customRender();
    await waitFor(() => {
      const inputElement = screen.getByPlaceholderText(
        'enter your price'
      ) as HTMLInputElement;
      expect(inputElement).toBeInTheDocument();
      fireEvent.change(inputElement, { target: { value: '10' } });
      expect(inputElement.value).toBe('10');
    });
  });
  test('should display text Tag Amount all the time', async () => {
    customRender();
    await waitFor(() => {
      const element = screen.getByLabelText('Amount');
      expect(element).toBeInTheDocument();
    });
  });
  test('should display Tag Amount input when user type', async () => {
    customRender();
    await waitFor(() => {
      const inputElement = screen.getByPlaceholderText(
        'enter your amount'
      ) as HTMLInputElement;
      expect(inputElement).toBeInTheDocument();
      fireEvent.change(inputElement, { target: { value: '20' } });
      expect(inputElement.value).toBe('20');
    });
  });
  test('should display title incorrectly amount-produce based on input value when Add product', () => {
    render(
      <QueryClientProvider client={queryClient}>
        <MemoryRouter initialEntries={['/cicd/product/add']}>
          <AddEditProduct />
        </MemoryRouter>
      </QueryClientProvider>
    );

    const addProductText = screen.getByText('Add Product');
    expect(addProductText).toBeInTheDocument();

    const addButton = screen.getByRole('button', { name: 'Add' });
    expect(addButton).toBeInTheDocument();
    const nameInput = screen.getByTestId('name-product');
    const priceInput = screen.getByTestId('price-product');
    const amountInput = screen.getByTestId('amount-product');

    fireEvent.change(nameInput, { target: { value: 'longtg' } });
    fireEvent.change(priceInput, { target: { value: 10 } });
    fireEvent.change(amountInput, { target: { value: 'abc' } });
    fireEvent.click(addButton);

    const errorMessage = screen.queryByText(
      'Please enter valid numbers for Amount.'
    );
    expect(errorMessage).toBeInTheDocument();
  });

  test('should not display any message when type price correctly (price = 10)', () => {
    render(
      <QueryClientProvider client={queryClient}>
        <MemoryRouter initialEntries={['/cicd/product/add']}>
          <AddEditProduct />
        </MemoryRouter>
      </QueryClientProvider>
    );

    const nameInput = screen.getByTestId('name-product');
    const priceInput = screen.getByTestId('price-product');

    fireEvent.change(nameInput, { target: { value: 'longtg' } });
    fireEvent.change(priceInput, { target: { value: 10 } });

    const addButton = screen.getByRole('button', { name: 'Add' });
    fireEvent.click(addButton);

    const errorMessage = screen.queryByText(
      'Please enter valid numbers for Price.'
    );
    expect(errorMessage).not.toBeInTheDocument();
  });

  test('should display an error message when type invalid price (price = abc)', () => {
    render(
      <QueryClientProvider client={queryClient}>
        <MemoryRouter initialEntries={['/cicd/product/add']}>
          <AddEditProduct />
        </MemoryRouter>
      </QueryClientProvider>
    );
    const addButton = screen.getByRole('button', { name: 'Add' });
    const nameInput = screen.getByTestId('name-product');
    const priceInput = screen.getByTestId('price-product');

    fireEvent.change(nameInput, { target: { value: 'longtg' } });
    fireEvent.change(priceInput, { target: { value: 'abc' } });
    fireEvent.click(addButton);

    const errorMessage = screen.queryByText(
      'Please enter valid numbers for Price.'
    );
    expect(errorMessage).toBeInTheDocument();
  });

  test('should display an error message when type invalid price ( price = -1)', () => {
    render(
      <QueryClientProvider client={queryClient}>
        <MemoryRouter initialEntries={['/cicd/product/add']}>
          <AddEditProduct />
        </MemoryRouter>
      </QueryClientProvider>
    );

    const addButton = screen.getByRole('button', { name: 'Add' });
    const nameInput = screen.getByTestId('name-product');
    const priceInput = screen.getByTestId('price-product');

    fireEvent.change(nameInput, { target: { value: 'longtg' } });
    fireEvent.change(priceInput, { target: { value: -1 } });
    fireEvent.click(addButton);

    const errorMessage = screen.queryByText(
      'Please enter valid numbers for Price.'
    );
    expect(errorMessage).toBeInTheDocument();
  });

  test('should display renders Update Product text all the time', () => {
    render(
      <QueryClientProvider client={queryClient}>
        <MemoryRouter initialEntries={['/cicd/product/:id']}>
          <AddEditProduct />
        </MemoryRouter>
      </QueryClientProvider>
    );

    const addProductText = screen.getByText('Update Product');
    expect(addProductText).toBeInTheDocument();

    const addButton = screen.getByRole('button', { name: 'Update' });
    expect(addButton).toBeInTheDocument();
  });
});
