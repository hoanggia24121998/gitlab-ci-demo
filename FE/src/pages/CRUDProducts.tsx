import { useState } from 'react';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import { Link, useNavigate } from 'react-router-dom';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { headers } from '../constants/UserDetail';
import Table from '../components/Table/index';
import { deleteProductData, getProductAll } from '../services/data.api';
import Button from '../components/Button';

const CRUDProducts = () => {
  const [, setDialog] = useState({
    isOpen: false,
    type: 'add',
  });
  const history = useNavigate();

  const handleOpenAddDialog = (): void => {
    setDialog({
      isOpen: true,
      type: 'add',
    });
    history('/');
  };

  const handleOpenUpdateDialog = () => {
    setDialog({
      isOpen: true,
      type: 'update',
    });
  };

  const { data: productAll } = useQuery({
    queryKey: ['productAll'],
    queryFn: getProductAll,
    refetchInterval: 10000,
  });

  const queryClient = useQueryClient();
  const handleRefreshOnClick = () => {
    queryClient.invalidateQueries({
      queryKey: ['productAll'],
    });
  };

  const deleteProductMutation = useMutation({
    mutationFn: (id: number | string) => deleteProductData(id),
    onSuccess: () => {
      handleRefreshOnClick();
    },
  });

  const handleDelete = (id: number) => {
    deleteProductMutation.mutate(id);
  };
  const dataProductAll =
    productAll?.map((item) => {
      const { id, name, price, amount } = item;
      return {
        id,
        name,
        price,
        amount,
        action: (
          <>
            <Link
              to={`cicd/product/${id}`}
              className="mr-3 py-1 px-1 rounded bg-blue-500 text-white"
              onClick={handleOpenUpdateDialog}
            >
              <EditIcon data-testid="Edition-icon" />
            </Link>

            <button
              type="button"
              className="mr-3 py-1 px-1 rounded bg-red-500 text-white"
              onClick={() => handleDelete(id as number)}
            >
              <DeleteIcon data-testid="Delete-icon" />
            </button>
          </>
        ),
      };
    }) || [];

  return (
    <div className="w-full h-full flex flex-col no-underline">
      <div className="flex items-center justify-between">
        <div className="col-start-2 m-3">
          <img
            className="h-auto max-w-xs"
            src="./ToshibaLogo.png"
            alt="Logo"
          />
        </div>
        <div className="flex-grow font-semibold text-center text-5xl">
          Products Page
        </div>
        <div className="col-start-2">
          <Button
            type="primary"
            link="cicd/product/add"
            handleOnClick={handleOpenAddDialog}
            label="Add product"
          />
        </div>
      </div>
      <div className=" flex no-underline text-center justify-center content-center">
        <Table
          headers={headers}
          dataTable={dataProductAll}
        />
      </div>
    </div>
  );
};

export default CRUDProducts;
