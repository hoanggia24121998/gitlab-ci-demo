/* eslint-disable jsx-a11y/label-has-associated-control */
import { useMutation, useQuery } from '@tanstack/react-query';
import { useMatch, useNavigate, useParams } from 'react-router-dom';
import React, { useState } from 'react';
import { Product } from '../types/ProductAll';
import {
  getProduct,
  pathProductData,
  postNewProduct,
} from '../services/data.api';

type ProductData = {
  name: string;
  price: number;
  amount: number;
};

const initialFormState = {
  name: '',
  price: 0,
  amount: 0,
};

const AddEditProduct = () => {
  const [formState, setFormState] = useState<ProductData>(initialFormState);
  const [isSubmitClicked, setIsSubmitClicked] = useState<boolean>(false);
  const addMatch = useMatch('cicd/product/add');
  const isAddMode = Boolean(addMatch);
  const { id } = useParams();
  const history = useNavigate();

  useQuery({
    queryKey: ['product', id],
    queryFn: () => getProduct(id as string),
    enabled: id !== undefined,
    onSuccess: (data) => {
      setFormState(data);
    },
  });

  const { mutate } = useMutation({
    mutationFn: (addProduct: ProductData) => {
      return postNewProduct(addProduct);
    },
    onSuccess: () => {
      setFormState(initialFormState);
    },
    onError: () => {},
  });

  const updateProductMutation = useMutation<ProductData, unknown, string>({
    mutationFn: () => pathProductData(id as string, formState as Product),
    onSuccess: () => {},
    onError: () => {},
  });
  const handleChange =
    (name: keyof ProductData) => (event: React.FormEvent<HTMLInputElement>) => {
      const { value }: any = event.target;

      let numericValue: string | number = value;

      if (name === 'price' || name === 'amount') {
        numericValue = /^[+-]?\d+(\.\d+)?$/.test(value)
          ? parseFloat(value)
          : value;
      }

      setFormState((prev) => ({ ...prev, [name]: numericValue }));
    };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setIsSubmitClicked(true);
    const { name, price, amount } = formState;
    const numericPrice: number = parseFloat(price as unknown as string);
    const numericAmount: number = parseFloat(amount as unknown as string);

    if (
      Number.isNaN(numericPrice) ||
      Number.isNaN(numericAmount) ||
      numericPrice <= 0 ||
      numericAmount <= 0
    ) {
      //
    } else {
      const updatedData = {
        name,
        price: numericPrice,
        amount: numericAmount,
      };

      if (isAddMode) {
        mutate(updatedData, {
          onSuccess: () => {
            history('/');
            setFormState(initialFormState);
          },
          onError: () => {},
        });
      } else {
        updateProductMutation.mutate('updatedData', {
          onSuccess: () => {
            history('/');
          },
          onError: () => {},
        });
      }
    }
  };
  return (
    <div className="border-4">
      <h1 className="text-lg ml-5">{isAddMode ? 'Add' : 'Update'} Product</h1>
      <form
        className="mt-6 ml-5 border-solid"
        onSubmit={handleSubmit}
      >
        <div className="group relative z-0 mb-6 w-full">
          <label
            htmlFor="name-product"
            className="mr-4 py-2 px-4 rounded bg-blue-500 text-white"
          >
            Name
          </label>
          <input
            type="text"
            name="name-product"
            id="name-product"
            data-testid="name-product"
            className="mr-4 py-2 px-4 rounded bg-grey-500 text-black border-2"
            placeholder="enter your name"
            value={formState.name}
            onChange={handleChange('name')}
            required
          />
        </div>
        <div className="group relative z-0 mb-6 w-full">
          <label
            htmlFor="price-product"
            className="mr-4 py-2 px-4 rounded bg-blue-500 text-white"
          >
            Price
          </label>
          <input
            type="text"
            name="price-product"
            id="price-product"
            data-testid="price-product"
            className="mr-4 py-2 px-4 rounded bg-grey-500 text-black border-2"
            placeholder="enter your price"
            value={formState.price}
            onChange={handleChange('price')}
            required
          />
          {isSubmitClicked &&
            !/^\d+$/.test(formState.price as unknown as string) && (
              <p className="text-red-500">
                Please enter valid numbers for Price.
              </p>
            )}
        </div>
        <div className="group relative z-0 mb-6 w-full">
          <label
            htmlFor="amount-product"
            className="mr-4 py-2 px-4 rounded bg-blue-500 text-white"
          >
            Amount
          </label>
          <input
            type="text"
            name="amount-product"
            id="amount-product"
            data-testid="amount-product"
            className="mr-4 py-2 px-4 rounded bg-grey-500 text-black border-2"
            placeholder="enter your amount"
            value={formState.amount}
            onChange={handleChange('amount')}
            required
          />
          {isSubmitClicked &&
            !/^\d+$/.test(formState.amount as unknown as string) && (
              <p className="text-red-500">
                Please enter valid numbers for Amount.
              </p>
            )}
        </div>

        <button
          type="submit"
          className="w-full rounded-lg bg-blue-700 px-5 py-2.5 text-center text-sm font-medium text-white hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 sm:w-auto"
        >
          {isAddMode ? 'Add' : 'Update'}
        </button>
      </form>
    </div>
  );
};
export default AddEditProduct;
