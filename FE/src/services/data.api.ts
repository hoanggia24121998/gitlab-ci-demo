import { PatchProductAll, Product, ProductAll } from '../types/ProductAll';
import { CreatProduct } from '../types/CreatProduct';
import http from '../utils/http';

export const getProductAll = async () => {
  const result = await http.get<ProductAll, ProductAll>('/cicd/product/all');
  return result;
};

export const getProduct = async (id: string | number) => {
  const result = await http.get<Product, Product>(`/cicd/product/${id}`);
  return result;
};

export const postNewProduct = async (addProduct) => {
  const result = await http.post<CreatProduct, CreatProduct>(
    '/cicd/product',
    addProduct
  );
  return result;
};

export const pathProductData = async (
  id: number | string,
  product: PatchProductAll
) => {
  const result = await http.patch<PatchProductAll, PatchProductAll>(
    `/cicd/product/${id}`,
    product
  );
  return result;
};

export const deleteProductData = async (id: number | string) => {
  const result = await http.delete<object>(`/cicd/product/${id}`);
  return result;
};
