import { useRoutes } from 'react-router-dom';
import CRUDProducts from '../pages/CRUDProducts';
import AddEditProduct from '../pages/AddEditProduct';

const AppRouter = () => {
  const element = useRoutes([
    {
      path: '/',
      element: <CRUDProducts />,
    },
    {
      path: 'cicd/product/:id',
      element: <AddEditProduct />,
    },
    {
      path: 'cicd/product/add',
      element: <AddEditProduct />,
    },
  ]);

  return element;
};

export default AppRouter;
