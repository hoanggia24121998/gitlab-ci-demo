export interface Environment {
  BASE_URL: string;
  MODE: string;
}
