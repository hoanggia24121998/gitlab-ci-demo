import { ProductAll } from './ProductAll';

export interface UpdateProductProps {
  key: keyof ProductAll;
  name: string;
  price: number;
  amount: number;
}
