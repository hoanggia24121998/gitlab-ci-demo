export interface TableProps {
  headers: string[] | null;
  dataTable?: Record<string, any>[];
}
