export type ProductAll = Product[];

export interface Product {
  id: number;
  name: string;
  price: number;
  amount: number;
  action: React.ReactNode;
}

export type PatchProductAll = Omit<Product, 'id' | 'action'>;
