import { render, screen } from '@testing-library/react';
import Button from '.';

describe('Should display button with label', () => {
  test('Should display button with label = abc', () => {
    render(
      <Button
        label="abc"
        type="primary"
      />
    );
    expect(screen.queryByText('abc')).toBeInTheDocument();
  });

  test('Should display button with label = 123', () => {
    render(
      <Button
        label="123"
        type="primary"
      />
    );
    expect(screen.queryByText('123')).toBeInTheDocument();
  });
});

describe('Should display button with proper type', () => {
  test('Should display button primary [background = blue]', () => {
    render(
      <Button
        label="abc"
        type="primary"
      />
    );
    expect(screen.queryByText('abc')).toHaveStyle(`backgroundColor: "blue"`);
  });

  test('Should display button secondary [background = red]', () => {
    render(
      <Button
        label="abc"
        type="secondary"
      />
    );

    expect(screen.queryByText('abc')).toHaveStyle(`backgroundColor: "blue"`);
  });
});

describe('Should apply style css correctly', () => {
  test('Should apply style css correctly with primary type', () => {
    render(
      <Button
        label="abc"
        type="primary"
      />
    );
    expect(screen.queryByText('abc')).toHaveClass(
      'mr-4 py-2 px-4 rounded text-white'
    );
    expect(screen.queryByText('abc')).toHaveStyle({
      'background-color': 'rgb(0, 0, 255)',
    });
  });

  test('Should apply style css correctly with secondary type', () => {
    render(
      <Button
        label="abc"
        type="secondary"
      />
    );
    expect(screen.queryByText('abc')).toHaveClass(
      'mr-4 py-2 px-4 rounded text-white'
    );

    expect(screen.queryByText('abc')).toHaveStyle({
      'background-color': 'rgb(255, 0, 0)',
    });
  });
});
