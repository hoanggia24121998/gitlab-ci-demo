type Props = {
  label: string;
  type: 'primary' | 'secondary';
  // eslint-disable-next-line react/require-default-props
  handleOnClick?: () => void;
  link?: string;
};

const Button = ({
  handleOnClick,
  label,
  link = 'cicd/product/add',
  type,
}: Props) => {
  return (
    <a
      role="button"
      className="mr-4 py-2 px-4 rounded text-white"
      style={{ background: type === 'secondary' ? 'red' : 'blue' }}
      href={link}
      data-testid="button-add"
      onClick={handleOnClick ? () => handleOnClick : undefined}
    >
      {label}
    </a>
  );
};

Button.defaultProps = {
  link: 'cicd/product/add',
};

export default Button;
