import { render, screen } from '@testing-library/react';
import Table from '.';

const headers = ['id', 'Name', 'Content'];
const data = [
  {
    id: 1,
    name: 'A',
    Content: 'Content-A',
  },
  { id: 2, name: 'B', Content: 'Content-B' },
  { id: 3, name: 'C', Content: 'Content-C' },
  { id: 4, name: 'D', Content: 'Content-D' },
];

describe('Table headers + rows', () => {
  beforeEach(() => {
    render(
      <Table
        headers={headers}
        dataTable={data}
      />
    );
  });

  test('should display table all the time', () => {
    expect(screen.getByRole('table')).toBeInTheDocument();
  });

  test('should display table with expected columns', () => {
    const columns = screen.getAllByRole('columnheader');
    expect(columns.length).toBe(3);
  });

  test('should display table with expected rows', () => {
    const rows = screen.getAllByRole('row');
    expect(rows.length).toBe(5);
  });

  test('should display table with expected cells', () => {
    const cells = screen.getAllByRole('cell');
    expect(cells.length).toBe(12);
  });
  test('should display comlumn with text', () => {
    const firstColumn = screen.getByRole('columnheader', { name: headers[0] });
    const secondColumn = screen.getByRole('columnheader', { name: headers[1] });
    const thirdColumn = screen.getByRole('columnheader', { name: headers[2] });
    expect(firstColumn).toBeDefined();
    expect(secondColumn).toBeDefined();
    expect(thirdColumn).toBeDefined();
  });

  test('should display cells with text', () => {
    const row = data[0];
    const values = Object.values(row);
    const firstCell = screen.getByRole('cell', { name: String(values[0]) });
    const secondCell = screen.getByRole('cell', { name: String(values[1]) });
    const thirdCell = screen.getByRole('cell', { name: String(values[2]) });
    expect(firstCell).toBeDefined();
    expect(secondCell).toBeDefined();
    expect(thirdCell).toBeDefined();
  });
});
describe('Table style with number of default row', () => {
  beforeEach(() => {
    render(
      <Table
        headers={headers}
        dataTable={data}
      />
    );
  });

  test('should display table with expected rows', () => {
    const rows = screen.getAllByRole('row');
    expect(rows.length).toBe(5);
  });

  test('should display table with expected cells', () => {
    const cells = screen.getAllByRole('cell');
    expect(cells.length).toBe(12);
  });
});
describe('Table style with no number of default row and auto height', () => {
  beforeEach(() => {
    render(
      <Table
        headers={headers}
        dataTable={data}
      />
    );
  });

  test('should display table with expected rows', () => {
    const rows = screen.getAllByRole('row');
    expect(rows.length).toBe(5);
  });

  test('should display table with expected cells', () => {
    const cells = screen.getAllByRole('cell');
    expect(cells.length).toBe(12);
  });
});
