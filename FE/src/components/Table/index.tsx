import clsx from 'clsx';
import { memo } from 'react';
import { TableProps } from '../../types/TableProps';

const Table = ({ headers, dataTable }: TableProps) => {
  return (
    <div className="w-full h-full flex flex-col shadow-3xl overflow-hidden rounded text-lg top-2">
      <div className="relative overflow-x-auto bg-slate-200 w-full h-full">
        <table className="w-full h-[100px] text-lg text-left text-gray-500  overflow-auto">
          <thead className="text-2xl text-gray-700 uppercase bg-gray-100  sticky top-0 h-12 z-20 shadow-sm">
            <tr className="relative">
              {headers?.map((header, index) => {
                return (
                  <th
                    key={header}
                    scope="col"
                    className={clsx('px-6 py-3 text-center', {
                      'sticky left-0 z-20 bg-gray-100': index === 0,
                    })}
                  >
                    {header}
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody>
            {dataTable &&
              dataTable.map((item, i) => {
                return (
                  <tr
                    key={i as number}
                    className="bg-white border-b hover:bg-slate-50 cursor-pointer group"
                  >
                    {Object.keys(item).map((value, index) => (
                      <td
                        key={value}
                        className={clsx('pl-2 text-center', {
                          'sticky left-0 z-10 bg-white group-hover:bg-slate-50 cursor-pointer whitespace-nowrap':
                            index === 0,
                        })}
                      >
                        {item[value]}
                      </td>
                    ))}
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default memo(Table);
