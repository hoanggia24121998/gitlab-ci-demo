import * as React from 'react';
import Button from '@mui/material/Button';
import MuiDialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return (
    <Slide
      direction="up"
      ref={ref}
      /* eslint-disable react/jsx-props-no-spreading */
      {...props}
    />
  );
});

type Props = {
  isOpen: boolean;
  handleDialog: (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => void;
  title: string;
  content: string;
};

const Dialog = ({ isOpen, handleDialog, title, content }: Props) => {
  return (
    <div>
      <Button
        variant="outlined"
        onClick={handleDialog}
      >
        Slide in alert dialog
      </Button>
      <MuiDialog
        open={isOpen}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleDialog}
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            {content}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDialog}>Disagree</Button>
          <Button onClick={handleDialog}>Agree</Button>
        </DialogActions>
      </MuiDialog>
    </div>
  );
};
export default Dialog;
