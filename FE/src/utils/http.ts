import axios, { AxiosInstance } from 'axios';
import { Environment } from 'types/Environment';

declare global {
  interface Window {
    ENV: Environment;
  }
}

export const BASE_URL = import.meta.env.VITE_BASE_URL;

class Http {
  instance: AxiosInstance;

  constructor() {
    this.instance = axios.create({
      baseURL: BASE_URL,
      timeout: 10000,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
}

const http = new Http().instance;
// Add a response interceptor
http.interceptors.response.use(
  // eslint-disable-next-line func-names
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response?.data;
  },
  // eslint-disable-next-line func-names
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);

export default http;
