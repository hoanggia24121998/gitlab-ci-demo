import { rest } from 'msw';
import getProductEx from './getProductEx.json';

const handlers = [
  rest.get('http://localhost:8080/cicd/product/all', (_req, res, ctx) => {
    return res(ctx.status(200), ctx.json(getProductEx));
  }),
];

export default handlers;
