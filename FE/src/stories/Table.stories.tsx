import type { Meta, StoryObj } from '@storybook/react';
import Table from '../components/Table';

const meta: Meta<typeof Table> = {
  component: Table,
};

export default meta;
type Story = StoryObj<typeof Table>;

export const Primary: Story = {
  args: {
    headers: ['ID', 'Name', 'Address'],
    dataTable: [{ id: 1, name: 'Gia', addr: 'VL' }],
  },
};
