import type { Meta, StoryObj } from '@storybook/react';

import Dialog from '../components/Dialog';

const meta: Meta<typeof Dialog> = {
  component: Dialog,
};

export default meta;
type Story = StoryObj<typeof Dialog>;

export const Default: Story = {
  argTypes: {
    isOpen: {
      options: [true, false],
      defaultValue: true,
      control: { type: 'radio' },
    },
  },
  args: {
    isOpen: true,
    title: 'Dialog Title',
    content: 'This is Dialog content',
  },
};
