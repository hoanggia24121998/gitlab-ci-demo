// Button.stories.ts|tsx

import type { Meta, StoryObj } from '@storybook/react';

import Button from '../components/Button';

const meta: Meta<typeof Button> = {
  component: Button,
};

export default meta;
type Story = StoryObj<typeof Button>;

export const Primary: Story = {
  argTypes: {
    type: {
      options: ['primary', 'secondary'],
      defaultValue: 'primary',
      control: { type: 'radio' },
    },
  },
  args: {
    label: 'Button',
    type: 'primary',
  },
};

export const Secondary: Story = {
  argTypes: {
    type: {
      options: ['primary', 'secondary'],
      control: { type: 'radio' },
    },
  },
  args: {
    label: 'Button',
    type: 'secondary',
  },
};
