/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      fontFamily: {
        'noto-san': ['Noto Sans JP', 'system-ui', 'sans-serif']
      },
      boxShadow: {
        '3xl': '0px 3px 6px #00000029'
      },
      colors: {
        'blue-button-default': 'rgb(25, 118, 210)'
      },
      gridTemplateColumns: {
        16: 'repeat(16, minmax(0, 1fr))'
      }
    },
    plugins: []
  }
}
