import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
  reporter: 'junit',
  reporterOptions: {
    mochaFile: 'cypress/results/e2e-output.xml',
    toConsole: true,
  },
  video: true,
  videoCompression: 15,
});
