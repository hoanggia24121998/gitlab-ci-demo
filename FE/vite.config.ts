import { defineConfig } from 'vite';
import { configDefaults } from 'vitest/config';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  test: {
    setupFiles: ['./vitest.setup.ts'],
    globals: true,
    environment: 'jsdom',
    reporters: ['basic', 'junit'],
    outputFile: 'junit.xml',
    deps: {
      optimizer: {
        web: {
          include: ['vitest-canvas-mock'],
        },
      },
    },
    coverage: {
      provider: 'v8',
      reporter: ['text', 'text-summary', 'html'],
      reportsDirectory: './tests/unit/coverage',
      exclude: [
        'src/utils/**',
        'src/context/**',
        'src/constants/**',
        'src/config/**',
      ],
      include: [
        // 'src/components/GaugeChart/**',
        'src/components/Table/**',
        // 'src/components/Box/**',
        'src/pages/**',
        // 'src/components/SubSidebar/**',
        // 'src/pages/CompletedTracking/**',
        // 'src/pages/WorkPerformance/**',
        // 'src/pages/BusinessPerformance/**',
        // 'src/pages/MainScreen/**',
        // 'src/pages/MachineStatusMap/**',
      ],
    },
    exclude: [...configDefaults.exclude, './src/utils/**', './src/services/**'],
    environmentOptions: {
      jsdom: {
        resources: 'usable',
      },
    },
  },
});
