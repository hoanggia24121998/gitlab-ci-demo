describe('Add new function', () => {
  it('should add data row [name = lifeboy] successfully', () => {
    cy.visit('http://localhost:5173');
    cy.get('a[data-testid=button-add]').click();
    cy.url().should('include', '/cicd/product/add');
    cy.get('input[name=name-product]').type('lifeboy');
    cy.get('input[name=price-product]').type('53');
    cy.get('input[name=amount-product]').type('54');
    cy.get('button[type=submit]').click();
    cy.url().should('include', '/');
    cy.contains('td', 'lifeboy').should('exist');
  });

  it('should add data row [name = Colgate] successfully', () => {
    cy.visit('http://localhost:5173');
    cy.get('a[data-testid=button-add]').click();
    cy.url().should('include', '/cicd/product/add');
    cy.get('input[name=name-product]').type('Colgate');
    cy.get('input[name=price-product]').type('53');
    cy.get('input[name=amount-product]').type('54');
    cy.get('button[type=submit]').click();
    cy.url().should('include', '/');
    // cy.visit('http://localhost:5173');
    cy.contains('td', 'Colgate').should('exist');
  });
});

describe('Remove function', () => {
  it('should remove data row [name = lifeboy]  successfully', () => {
    cy.visit('http://localhost:5173');
    cy.contains('td', 'lifeboy').parent().find('button').click();
    cy.reload();
    cy.contains('td', 'lifeboy').should('not.exist');
  });

  it('should remove data row [name = Colgate] successfully', () => {
    cy.visit('http://localhost:5173');
    cy.contains('td', 'Colgate').parent().find('button').click();
    cy.reload();
    cy.contains('td', 'Colgate').should('not.exist');
  });
});
